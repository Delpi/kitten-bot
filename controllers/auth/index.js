module.exports = function authHandler (tools) {
  return async function (criterion = 'any', identifiers) {
    // NOTE: criterions are sorted by *increasing* exclusivity
    // a lower permission set grants access to every higher role

    switch (criterion.toLocaleLowerCase()) {
      case 'any':
        return true;

      case 'mod':
      case 'moderator':
        // use the query handler in tools
        // return true if authorized

      case 'admin':
      case 'administrator':
        // use the query handler in tools
        // return true if authorized

      case 'owner':
        if (identifiers.userId === identifiers.ownerId) {
          return true
        }

      case 'mantainer':
        if (tools.config?.adminUsers?.find(
          adminId => adminId === identifiers.userId
        )) {
          return true;
        }
    }
    return false
  };
};