const fs = require('node:fs');

const queries = [];

fs.readdirSync(__dirname)
  .forEach((file) => {
    if (file.includes('index.js')) {
      return;
    }

    const query = require(`./${file}`);

    if (typeof(query) !== 'function') {
      console.log(`Skipping query in ${file}: invalid format`);
      return;
    }

    queries.push({
      generator: query,
      name: file.split('.')[0]
    });
  });

module.exports = function QueryHandler ({ models }) {
  const generatedQueries = {};
  queries.forEach(query => {
    generatedQueries[query.name] = query.generator(models);
  });

  return generatedQueries;
};