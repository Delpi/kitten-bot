module.exports = function channels ({ Channels }) {
  return {
    registerChannelRole: async (server, channel, role) => {
      try {
        console.log(`Registering a new ${role} channel in server ${server}`);
        const newChannel = await Channels.create({
          role,
          channel,
          server
        });

        return newChannel;

      } catch (err) {
        console.warn(`Failed to assign a role channel for server ${server}: ${err.message}`);
        return;
      }
    },

    fetchChannelByRole: async (server, role) => {
      try {
        console.log(`Attempting to find ${role} channel in server ${server}`);
        const channel = await Channels.findOne({
          where: { server, role }
        });

        return channel.dataValues;
      } catch (err) {
        console.warn(`Failed to run query for ${role} channel in server ${server}: ${err.message}`);
        return;
      }
    }

  };
};