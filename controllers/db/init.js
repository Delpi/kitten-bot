const { Sequelize } = require('sequelize');
const instance = require('../../models');

module.exports = async function init () {
  const db = new Sequelize({
    dialect: 'sqlite',
    storage: 'kitten.db',
  });

  try {
    await db.authenticate();
    await instance(db);

    return db;

  } catch (err) {
    console.error('Unable to connect to db:', err);
  }
};