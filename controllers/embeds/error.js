const { EmbedBuilder } = require('discord.js');
const config = require('../../config.json');

module.exports = function embed (severity, message) {
  const resEmbed = new EmbedBuilder()
    .setTitle('Error!')
    .setColor(
      severity?.toLowerCase() === 'error' ?
      config.cosmetics.palette.error :
      config.cosmetics.palette.warn
    )
    .setDescription(message);

  return { embeds: [resEmbed], ephemeral: true };
};