const { EmbedBuilder } = require('discord.js');
const config = require('../../config.json');

module.exports = function embed (title, fields = [], isPermanent = false) {
  const resEmbed = new EmbedBuilder()
    .setTitle(title)
    .setColor(config.cosmetics.palette.main)
    .addFields([ ...fields ]);

  return { embeds: [resEmbed], ephemeral: !isPermanent };
};