module.exports = function userId (userObj) {
  return {
    id: userObj.id,
    tag: `<@${userObj.id}>`,
    name: userObj.username,
    displayName: (
      userObj.discriminator && userObj.discriminator != 0 ?
      `${userObj.username}#${userObj.discriminator}` :
      `@${userObj.username}`
    ),
  };
};