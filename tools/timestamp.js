module.exports = function timestamp (unixTimestamp, format = 'F') {
  const time = Math.floor(
    unixTimestamp / 1000
  );
  return `<t:${time}:${format}>`;
};