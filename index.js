// Discord dependencies
const { Client, GatewayIntentBits } = require('discord.js');
const { REST } = require('@discordjs/rest');

// Utils
const initDb = require('./controllers/db/init');
const queryDb = require('./controllers/db/query');
const initCommands = require('./commands');
const initEvents = require('./events');

// Settings
const config = require('./config.json');

// Pre-initialization declarations
const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMembers,
    GatewayIntentBits.GuildPresences,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMessageReactions
  ]
});

const rest = new REST({ version: '9' })
  .setToken(config.botToken);

async function init () {
  try {
    console.log("Initializing database");
    const db = await initDb();

    console.log('Generating QueryDB utility');
    const QueryHandler = queryDb(db);

    console.log('Registering bot commands');
    initCommands(client, QueryHandler);

    console.log('Registering bot events');
    initEvents(client, QueryHandler);

  } catch (err) {
    console.error(err);
  } finally {
    await client.login(config.botToken);
    console.log('Client is loaded');
  }
}

// Init

init();