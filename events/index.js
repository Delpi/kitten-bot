const { Events } = require('discord.js');

const fs = require('node:fs');

const errorEmbed = require('../controllers/embeds/error');

const config = require('../config.json');

const events = [];

fs.readdirSync(__dirname)
  .forEach((file) => {
    if (file.includes('index.js')) {
      return;
    }

    const event = require(`./${file}`);
    events.push({
      data: event,
      fileName: file,
    });
  });

module.exports = async function initEvents (client, queryHandler) {
  const tools = {
    client,
    queryHandler,
    config
  };

  console.log('Parsing event files');

  events.forEach((event) => {
    if (typeof event.data !== 'function') {
      console.warn(
        `Skipping an event: ${event.fileName} does not export a function`
      );
      return;
    }

    const newEvent = event.data(tools);

    if (!newEvent.event || !newEvent.execute) {
      console.warn(
        `Skipping an event: ${event.fileName} returns an invalid format`
      );
      return;
    }

    if (newEvent.once) {
      client.once(
        Events[newEvent.event],
        newEvent.execute
      );
    } else {
      client.on(
        Events[newEvent.event],
        newEvent.execute
      );
    }
  });

  console.log(`Successfully registered ${events.length} event files`);
};