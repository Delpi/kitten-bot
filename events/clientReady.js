module.exports = function clientReady (tools) {
  return {
    once: true,
    event: 'ClientReady',
    execute: async (client) => {
      console.log(`Logged in as ${client.user.tag}`)
    },
  };
};