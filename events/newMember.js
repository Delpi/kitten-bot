const utilityReply = require('../controllers/embeds/utilityReply');

const userId = require('../tools/userId');

function channelRoleMessage (role, user) {
  switch (role) {
    case 'welcome':
      return `Welcome to the server, ${user.tag}`;

    case 'modlog':
      return utilityReply(
        'New member',
        [
          {
            name: 'Name',
            value: user.displayName
          },
          {
            name: 'Id',
            value: user.id
          }
        ]
      );

    default:
      return;
  }
}

module.exports = function newMember (tools) {
  return {
    once: false,
    event: 'GuildMemberAdd',
    execute: async (guildMember) => {
      const user = userId(guildMember.user);
      console.log(
        `New member in server id ${guildMember.guild.id}:`,
        user.displayName,
        user.id
      );

      const [welcome, log] = await Promise.all(
        ['welcome', 'modlog'].map(r => tools
          .queryHandler
          .channels
          .fetchChannelByRole(
            guildMember.guild.id, r
          ))
      );

      await Promise.all(
        [welcome, log].map(async c => {
          if (!c) {
            return;
          }

          tools
            .client
            .channels
            .cache
            .get(c.channel)
            ?.send(
              channelRoleMessage(c.role, user)
            );
        })
      );
    },
  };
};