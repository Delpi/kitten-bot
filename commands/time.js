const utilityReply = require('../controllers/embeds/utilityReply');

const timestamp = require('../tools/timestamp');

function formatTimePart (part) {
  if (!part) {
      return undefined;
  }
  return part < 10 ? `0${part}` : `${part}`;
}

module.exports = function time (tools) {
  return {
    data: new tools.SlashCommandBuilder()
      .setName('time')
      .setDescription('Outputs an universal timestamp')
      .addStringOption(o =>
        o.setName('format')
          .setDescription('Sets the output format of the timestamp')
          .addChoices(
            { name: 'Date, short', value: 'd' },
            { name: 'Date, long', value: 'D' },
            { name: 'Time, short', value: 't' },
            { name: 'Time, long', value: 'T' },
            { name: 'Full, short', value: 'f' },
            { name: 'Full, long', value: 'F' },
            { name: 'Relative', value: 'R' },
      ))
      .addIntegerOption(o =>
        o.setName('year')
          .setDescription('Year of the output date, defaults to current')
          .setMinValue(1971)   // Limits set by UNIX epoch,
          .setMaxValue(2037))  // plus/minus padding
      .addIntegerOption(o =>
        o.setName('month')
          .setDescription('month of the output date, defaults to current')
          .setMinValue(1)
          .setMaxValue(12))
      .addIntegerOption(o =>
        o.setName('day')
          .setDescription('Day of the output date, defaults to current')
          .setMinValue(1)
          .setMaxValue(31))
      .addIntegerOption(o =>
        o.setName('hour')
          .setDescription('Hour of the output time, defaults to current')
          .setMinValue(0)
          .setMaxValue(23))
      .addIntegerOption(o =>
        o.setName('minute')
          .setDescription('Minute of the output time, defaults to current')
          .setMinValue(0)
          .setMaxValue(59))
      .addIntegerOption(o =>
        o.setName('timezone')
          .setDescription('Timezone of the input, defaults to UTC (+0)')
          .setMinValue(-11)
          .setMaxValue(14)),

    execute: async (interaction) => {
      const { options } = interaction;

      const [format, timezone] = [
        options.getString('format'),
        options.getInteger('timezone') ?? 0
      ];

      const current = new Date();

      const year = options.getInteger('year') ?? current.getUTCFullYear();
      const month = formatTimePart(
        options.getInteger('month') ?? current.getUTCMonth() + 1
      );
      const day = formatTimePart(
        options.getInteger('day') ?? current.getUTCDate()
      );

      const hour = formatTimePart(
        options.getInteger('hour') ?? current.getUTCHours()
      );

      const minute = formatTimePart(
        options.getInteger('minute') ?? current.getUTCMinutes()
      );

      const second = '00'; // I assume it isn't worth it to input

      const isoTime = (
        `${year}-${month}-${day}T${hour}:${minute}:${second}.000Z`
      );

      const unixTimestamp = new Date(isoTime).getTime();
      const timezoneDifference = timezone * 3600000; // ms in timezone shift

      const targetDate = unixTimestamp - timezoneDifference;

      const finalizedTimestamp = timestamp(targetDate, format);

      await interaction.reply(
        utilityReply(
          'Time',
          [
            { name: 'Timestamp', value: `\` ${finalizedTimestamp}\``},
            { name: 'Outputs', value: finalizedTimestamp }
          ]
      ));
    },
  };
};