const utilityReply = require('../controllers/embeds/utilityReply');
const errorEmbed = require('../controllers/embeds/error');

const auth = require('../controllers/auth');

module.exports = function setupChannel (tools) {
  const authHandler = auth(tools);

  return {
    data: new tools.SlashCommandBuilder()
      .setName('config')
      .setDescription('Configures this channel for a given role')
      .addStringOption(o =>
        o.setName('role')
          .setDescription('Sets the role for the channel')
          .setRequired(true)
          .addChoices(
            { name: 'Welcome', value: 'welcome' },
            { name: 'Mod log', value: 'modlog' },
      )),
    execute: async (interaction) => {

      const isAuthorized = await authHandler(
        'admin',
        {
          guildId: interaction.guild.id,
          ownerId: interaction.guild.ownerId,
          userId: interaction.user.id,
          roles: interaction.member._roles
        }
      );

      if (!isAuthorized) {
        await interaction.reply(
          errorEmbed('warning', 'Not authorized to run this command')
        );
        return;
      }

      const role = interaction.options.getString('role');

      const newChannel = await tools
        .queryHandler
        .channels
        .registerChannelRole(
          interaction.guild.id,
          interaction.channelId,
          role
        );

      await interaction.reply(
        newChannel ?
        utilityReply(
          'Channel set up',
          [{ name: 'Success!', value: `Logged this channel for ${role}` }]
        ) :
        errorEmbed('error', 'Failed to set up channel')
      );
    },
  };
};