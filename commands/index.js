const { SlashCommandBuilder, Collection, Events, REST, Routes } = require('discord.js');

const fs = require('node:fs');

const errorEmbed = require('../controllers/embeds/error');

const config = require('../config.json');

const commands = [];

fs.readdirSync(__dirname)
  .forEach((file) => {
    if (file.includes('index.js')) {
      return;
    }

    const command = require(`./${file}`);
    commands.push({
      data: command,
      fileName: file,
    });
  });

module.exports = async function initCommands (client, queryHandler) {
  const tools = {
    SlashCommandBuilder,
    client,
    queryHandler,
    config
  };

  client.commands = new Collection();
  const commandsJson = [];

  console.log('Parsing command files');

  commands.forEach((command) => {
    if (typeof command.data !== 'function') {
      console.warn(
        `Skipping a command: ${command.fileName} does not export a function`
      );
      return;
    }

    const newCommand = command.data(tools);

    if (!newCommand.data || !newCommand.execute) {
      console.warn(
        `Skipping a command: ${command.fileName} returns an invalid format`
      );
      return;
    }

    client.commands.set(newCommand.data.name, newCommand);
    commandsJson.push(newCommand.data.toJSON());
  });

  try {
    console.log('Registering commands');

    const rest = new REST()
      .setToken(config.botToken);

    const data = await rest.put(
      Routes.applicationCommands(config.botClient),
      { body: commandsJson }
    );

    console.log(`Successfully registered ${data.length} application commands`);
  } catch (err) {
    console.error('Failed to register commands:', err);
  }

  console.log('Creating command events');

  client.on(Events.InteractionCreate, async (interaction) => {
    if (!interaction.isChatInputCommand()) {
      return;
    }

    const command = interaction.client.commands.get(
      interaction.commandName
    );

    if (!command) {
      console.warn(`No command matching ${interaction.commandName} found`);
      return;
    }

    try {
      await command.execute(interaction);
    } catch (err) {
      console.warn(err);
      const res = errorEmbed('warn', 'Unable to execute command');
      if (interaction.replied || interaction.deferred) {
        await interaction.followUp(res);
      } else {
        await interaction.reply(res);
      }
    }
  });
};