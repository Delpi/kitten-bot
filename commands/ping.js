module.exports = function ping (tools) {
  return {
    data: new tools.SlashCommandBuilder()
      .setName('ping')
      .setDescription('pong'),

    execute: async (interaction) => {
      await interaction.reply('Pong!');
    },
  };
};