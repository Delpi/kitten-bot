const { DataTypes } = require('sequelize');

const name = 'Roles';

module.exports = {
  name,
  define: (db) => {
    db.define(name, {
      recordId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      role: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      group: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      server: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    });
  }
};