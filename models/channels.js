const { DataTypes } = require('sequelize');

const name = 'Channels';

module.exports = {
  name,
  define: (db) => {
    db.define(name, {
      recordId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      role: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      channel: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      server: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    });
  }
};