const fs = require('node:fs');

const models = [];

fs.readdirSync(__dirname)
  .forEach((file) => {
    if (file.includes('index.js')) {
      return;
    }

    const model = require(`./${file}`);
    models.push({
      name: model.name,
      define: model.define
    });
  });

module.exports = async function instance (db) {
  models.forEach((model) => {
    model.define(db);
  });

  // NOTE: only execute if any model was changed
  const updateDb = false;
  if (updateDb) {
    await db.sync({
      force: false,
      alter: true,
    });
  }
}